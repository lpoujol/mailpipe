#!/usr/bin/php
<?php

// Override Postfix default umask
umask(0027);

require_once __DIR__.'/vendor/autoload.php';

$shortopts  = "s:";  // Sender's email
$shortopts .= "r:";  // Recipient email
$options = getopt($shortopts);

$config = parse_ini_file(__DIR__."/config.ini");
$configured_pipes = parse_ini_file(__DIR__."/pipes.ini", true);
$selected_pipe = array();


$matches = array();
if (preg_match("/^mailpipe\+([A-Za-z0-9]+)@/", $options['r'], $matches)) {
    $pipe_id = $matches[1];

    if (isset($configured_pipes[$pipe_id])) {
        $selected_pipe = &$configured_pipes[$pipe_id];
    } else {
        echo "DROPPED - WRONG PIPE ID";
        exit(0);
    }

} else {
    echo "DROPED - NO PIPE SELECTED";
    exit(0);
}

// Now that we're sure to have a good pipe, let's parse that mail
$parser = new PhpMimeMailParser\Parser();
$parser->setStream(fopen("php://stdin", "r"));

$arrayHeaders = $parser->getHeaders();
$subject = $parser->getHeader('subject');
$text = $parser->getMessageBody('text');


$short_pipe_message = "From: ".$options['s']."
Date: ".$arrayHeaders['date']."
Subject: $subject

".substr($text, 0,  500)."

";


$long_pipe_message = $parser->getHeadersRaw();

$long_pipe_message .= "\n$text";

if($config['web_full_version']) {
  $mailid = bin2hex(random_bytes(16));

  if(!is_dir(__DIR__.'/www/'.$selected_pipe['uid'])) {
    mkdir(__DIR__.'/www/'.$selected_pipe['uid'], 0755);
  }

  file_put_contents(__DIR__.'/www/'.$selected_pipe['uid'].'/'.$mailid.'.txt', $long_pipe_message);

  $url = $config['web_main_url'].$selected_pipe['uid'].'/'.$mailid.'.txt';

} else {
  $url = false;
}



// Let's proceed actions as configured
foreach (explode(',', $selected_pipe['type']) as $pipe_type) {

    switch ($pipe_type) {
    case 'discord':
        pipe_to_discord($selected_pipe['discord_hook_url'], $short_pipe_message, $url);
        break;
    case 'log':
        pipe_to_log($selected_pipe['uid'], $long_pipe_message);
        break;

    default:
        echo "DROPPED - ERROR : UNKNOWN PIPE TYPE";
        exit(0);
      break;
    }

}


function pipe_to_log($log_name, $log_message)
{

    $log_file = fopen(__DIR__.'/log/'.$log_name, 'a+');
    fwrite($log_file, $log_message);
    fclose($log_file);

}


function pipe_to_discord($hook_url, $hook_message, $url = false)
{
  $header = ($url ? "Full version at $url \n" : "");

    $data_string = json_encode(
        array(
            "username"      =>  "MailPipe",
            "avatar_url"    =>  "https://i.imgur.com/PuirAoh.jpg",
            "content"       =>  "$header```$hook_message```",
        )
    );


    $ch = curl_init($hook_url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    curl_close($ch);
}
